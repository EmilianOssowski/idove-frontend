import {EventEmitter, Injectable, Output} from '@angular/core';
import {OauthService} from "./oauth.service";
import {User} from "../models/user.model";
import {CookieService} from "ngx-cookie-service";
import {ConfigurationService} from "./configuration.service";
import {HttpService} from "./http.service";
import 'rxjs/Rx';
import {Router} from "@angular/router";
import {PopupsService} from "./popups.service";
import {Branch} from "../models/branch.model";
import {Fancier} from "../models/fancier.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";


@Injectable()
export class UserService {
  private isUserLoggedIn = false;
  isLoginDisabled= false;
  private user : User;
  //User
  private idUser : number;
  private username : string ="";
  private password : string;
  private email : string;
  private confirmationToken : string ;
  private firstName : string;
  private lastName : string;
  private address : string ;
  private city : string ;
  private telephoneNumber : string ;
  private lastLogin = null;
  private isActive : boolean = false;
  private fancier : boolean = false;
  private calculator : boolean = false;
  //Fancier
  private idSection : number;
  private idDovecote : number;
  private teamName : string ;
  private fancierNumber : number;
  //Calculator
  private branchList : BehaviorSubject<Array<Branch>> = new BehaviorSubject<Array<Branch>>([]);

  refreshMapRequest : EventEmitter<boolean> = new EventEmitter<boolean>()


  constructor(private oauth : OauthService, private cookie : CookieService, private config : ConfigurationService, private http : HttpService , private router : Router ,private popup : PopupsService)  {
    this.isUserLoggedIn = false;
  }
  login(username : string, password : string){
    this.http.login(username, password).map((res : Response) => {
      return res;
    }).map((res : Response)=>
      {
      return res;
      }
    ).subscribe((res : Response)=>{
      this.idUser = res.userDb.idUser;
      this.username = res.userDb.username;
      this.password = res.userDb.password;
      this.email = res.userDb.email;
      this.confirmationToken = res.userDb.confirmationToken;
      this.firstName = res.userDb.firstName;
      this.lastName = res.userDb.lastName;
      this.address = res.userDb.address;
      this.city = res.userDb.city;
      this.telephoneNumber = res.userDb.telephoneNumber;
      this.teamName = res.userDb.teamName;
      this.lastLogin = res.userDb.lastLogin;
      this.isActive = res.userDb.status;
      this.fancier = res.userDb.fancier;
      this.calculator = res.userDb.calculator;
      console.log("user service");
      console.log(res)
      this.cookie.set("token", res.authToken.token);
      this.setUserLoggedIn();
      this.router.navigate(["/mainpage"]);
      this.popup.addSuccess('Zalogowano', 'Witaj '+ this.username);
      //wczytanie danych hodowcy
      if(this.fancier==true){
        this.http.getFancierDetails(res.authToken.token).subscribe( (res: Fancier) => {
          this.idSection = res.sectionId;
          this.idDovecote = res.dovecoteId;
          this.teamName = res.teamName;
          this.fancierNumber = res.fancierNumber;

        })
      }
      //wczytanie danych rachmistrza
      if(this.calculator==true){
        this.http.getBranchList(res.authToken.token).subscribe( (res : Array<Branch>)=>{
          this.branchList.next(res);
        })
      }
    }, error => {
      if(error.error.status = 401){
        this.popup.addError("Błąd" ,  "Błędny login lub hasło");
      }
      else{
        this.popup.addError("Błąd" ,  error.error.message);
      }
      this.isLoginDisabled=false;
    });
    //odświeżanie szczegolow usera
    this.http.refreshUserDetailsRequest.subscribe(
      ()=>{
        console.log("Odebrano UserDetailsRequest " + this.idDovecote);
        this.http.getUserByToken(this.getToken()).subscribe((res : User)=>{
          this.username = res.username;
          this.password = res.password;
          this.email = res.email;
          this.firstName = res.firstName;
          this.lastName = res.lastName;
          this.address = res.address;
          this.city = res.city;
          this.telephoneNumber = res.telephoneNumber;
          this.teamName = res.teamName;
          this.lastLogin = res.lastLogin;
          this.fancier = res.fancier;
          this.calculator = res.calculator;
          if(this.fancier==true){
            this.http.getFancierDetails(this.getToken()).subscribe( (res: Fancier) => {
              console.log("odświezanie szczegółów hodowcy " + res.dovecoteId)
              this.idSection = res.sectionId;
              this.idDovecote = res.dovecoteId;
              this.teamName = res.teamName;
              this.fancierNumber = res.fancierNumber;
              this.refreshMapRequest.emit()
            })
          }
        })
      }
    );

  }
  getBranchList(){return this.branchList.asObservable();  }
  getUserId(){return this.idUser }
  getUsername(){return this.username }
  getPassword(){return this.password }
  getEmail(){return this.email }
  getConfirmationToken(){return this.confirmationToken }
  getFirstName(){return this.firstName }
  getLastName(){return this.lastName }
  getIdSection(){return this.idSection }
  getIdDovecote(){return this.idDovecote }
  getAddress(){return this.address }
  getCity(){return this.city }
  getTelephoneNumber(){return this.telephoneNumber }
  getTeamName(){return this.teamName }
  getLastLogin(){return this.lastLogin }
  getStatus(){return this.isActive }
  getFancierNumber(){return this.fancierNumber }
  isFancier(){return this.fancier }
  isCalculator(){return this.calculator }




  setUserLoggedIn(){
    this.isUserLoggedIn = true;
  }
  getIsUserLoggedIn() {
    return this.isUserLoggedIn;
  }

  logout() {
    this.oauth.logout();
    this.isUserLoggedIn = false;
    this.user = null;
    this.idUser =null;
    this.username  ="";
    this.password = null;
    this.email = null;
    this.confirmationToken = null;
    this.firstName = null;
    this.lastName = null;
    this.address = null;
    this.city = null;
    this.telephoneNumber = null;
    this.lastLogin = null;
    this.isActive = null;
    this.fancier = false;
    this.calculator = false;
    this.idSection =null;
    this.idDovecote =null;
    this.teamName =null;
    this.fancierNumber =null;
    this.branchList  = new BehaviorSubject<Array<Branch>>([]);//
    this.isLoginDisabled = false;

  }

  isUserLogged() {
    if (this.oauth.checkCredentials() === true) {
      if (this.isUserLoggedIn === true) {
        return true;
      }
    } else { return false; }
  }
  getToken() {
    return this.cookie.get('token');
  }

  getFirstname() {
    return this.firstName;
  }
  getLastname() {
    return this.lastName;
  }

  changePassword( oldPassword, newPassword){
    this.http.changePassword(this.getToken(),  oldPassword, newPassword)

  }
  changeDetails(username, email, firstName, lastName, idSection, idDovecote, address, city, telephoneNumber, teamName, fancierNumber){
    const details = JSON.stringify({username, email, firstName, lastName, idSection, idDovecote, address, city, telephoneNumber, teamName, fancierNumber});
    console.log(details);
    this.http.changeDetails(this.getToken(), details)
  }
  remindPassword(mail){
    this.http.remindPassword(mail)
  }
  resetPassword(password, token){
    this.http.resetPassword(password, token)
    this.router.navigate(["/login"])
  }
}


export interface Response
{
  authToken: {
    token;
  },
  userDb: {
    idUser;
    username;
    password;
    email;
    confirmationToken;
    firstName;
    lastName;
    address;
    city;
    telephoneNumber;
    teamName;
    lastLogin;
    status;
    registrationDate;
    fancier;
    calculator;
  }
}


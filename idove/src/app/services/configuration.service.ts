import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Dovecote} from '../models/dovecote.model';
import {User} from '../models/user.model';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ConfigurationService {
  //nieużywane
  userObs = new BehaviorSubject<User>(new User(null,null,null,null,null,null,null,null,null,null,null) );
  constructor(private http : HttpService) {

  }
  update(token)   {
    this.http.getUserByToken(token).subscribe( (data: User)  => {
      this.userObs.next(data);
    });
  }
  getUserObs(token): Observable<User> {
    this.update(token);
    return this.userObs.asObservable();
  }



}

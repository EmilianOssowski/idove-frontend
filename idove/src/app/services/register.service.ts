import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../models/user.model";
import {UserService} from "./user.service";
import {Router} from "@angular/router";
import {PopupsService} from "./popups.service";

@Injectable()
export class RegisterService {

  constructor(private http: HttpClient, private router : Router , private popup : PopupsService) { }

  register(username, password, email, firstName, lastName, address, city, telephoneNumber){
  let user = new User(null,firstName,lastName,password,username,address,city,email,telephoneNumber,null,null);
    let headers = new HttpHeaders({'Content-type': 'application/json'});
    let body = JSON.stringify( user );
    console.log(user);

    this.http.post('http://localhost:8080/register', body , {headers: headers} )
      .map(res => res )
      .subscribe(
        res =>{
            this.popup.addSuccess("Rejestracja udana","Wysłano link potwierdzający na adres E-mail");
          },
          err => {
            this.popup.addError("Błąd", err.message)
        }
      );
  }

}


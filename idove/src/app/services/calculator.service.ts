import { Injectable } from '@angular/core';
import {SectionService} from "./section.service";
import {Section} from "../models/section.model";
import {Branch} from "../models/branch.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";
import {Observable} from "rxjs/Observable";
import {CFancier} from "../models/c-fancier.model";
import {Pigeon} from "../models/pigeon.model";


@Injectable()
export class CalculatorService {
  branch : Branch = new Branch(78,'083', 1, "gorzów", null)
  sectionsListObs =new  BehaviorSubject<Array<Section>>([]);
  fanciersListObs =new  BehaviorSubject<Array<CFancier>>([]);

  pigeonsListObs = new  BehaviorSubject<Array<Pigeon>>([]);
  constructor(private section : SectionService, private http: HttpService, private user : UserService) {
    this.http.refreshSectionsRequest.subscribe(()=>{
      this.refresh();
    })
    this.http.refreshFancierRequest.subscribe(()=>{
      this.refresh()
    })
    this.http.refreshPigeonsRequest.subscribe(res=>{
      this.getPigeonsByIdFancier(res)
    })
  }
  public isLoading = true;
  refresh() {
    this.isLoading = true;
    this.http.getSections(this.user.getToken(), this.branch.idBranch).subscribe(list => {
      this.sectionsListObs.next(list);
      console.log(list);
      this.isLoading = false;
    });
    this.isLoading = true;
    this.http.getObjectList(this.user.getToken(), "fancier/branch", this.branch.idBranch,'idBranch').subscribe(res =>{
      this.fanciersListObs.next(res);
      console.log(res);
      this.isLoading = false;
    })
  }
  getSectionsListObs() : Observable<Array<Section>>{
    return this.sectionsListObs.asObservable()
  }

  getFanciersListObs() : Observable<Array<CFancier>>{
    return this.fanciersListObs.asObservable()
  }


  setBranch(branch){
    this.branch = branch;
    this.refresh();
  }
  getBranchName(){
    return this.branch.name;
  }
  clearBranch(){
    this.branch = null;
    this.fanciersListObs.next(null);
    this.sectionsListObs.next([]);
  }
  addFancier(userId, sectionId, fancierNumber){
    this.http.addFancierToSection(this.user.getToken(), userId, sectionId, fancierNumber)

  }
  getFanciersByEmail(email){
    return this.http.getFanciersByEmail(this.user.getToken(),email)
  }

  addSection(name){
    this.section.addSection(this.branch.idBranch, name);
    console.log("refresh")
  }
  confirmDovecote(idDovecote){
    this.http.putObject(this.user.getToken(),"dovecote/confirm", null, idDovecote,'idDovecote')

  }
  confirmPigeon(idPigeon, idFancier?){
    this.http.putObject(this.user.getToken(), "pigeon/confirm", null, idPigeon, "pigeonId", idFancier)
  }

  getPigeonsByIdFancier(idFancier){
    this.http.getObjectList(this.user.getToken(), "pigeon", idFancier, 'fancierId').subscribe((res : Array<Pigeon>)=>{
      this.pigeonsListObs.next(res);
    })
  }
  getPigeonsListObs(){
    return this.pigeonsListObs.asObservable()
  }

}

import {EventEmitter, Injectable} from '@angular/core';
import {UserService} from './user.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Dovecote} from '../models/dovecote.model';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/Observable';
import {D} from "@angular/core/src/render3";

@Injectable()
export class DovecoteService {

  dovecoteObs = new BehaviorSubject<Dovecote>(new Dovecote(null, '', ''));
  dovecoteByIdObs = new BehaviorSubject<Dovecote>(new Dovecote(null, '', ''));
  constructor(private http: HttpService, private  user: UserService) {
    this.http.refreshDovecoteRequest.subscribe(()=>{
      console.log("odebrano event dovecoterefresh")
      this.refresh();
    })
    this.http.refreshDovecoteByIdRequest.subscribe(()=>{
      this.getDovecoteById(this.dovecoteByIdObs.value.idDovecote)
    })
  }

  getDovecoteObs(): Observable<Dovecote> {
    return this.dovecoteObs.asObservable();
  }
  addDovecote(body){
    this.http.postObject(this.user.getToken(), "dovecote", body)
  }

  editDovecote(dovecote : Dovecote){
    this.http.putObject(this.user.getToken(), "dovecote", JSON.stringify({adrdress:dovecote.adrdress, longtitude: dovecote.longtitude, latitude: dovecote.latitude, city: dovecote.city}), dovecote.idDovecote, "dovecoteId")
  }
  getDovecoteByIdObs(){
    return this.dovecoteByIdObs.asObservable();
  }

  getDovecoteById(idDovecote){
    this.http.getDovecote(this.user.getToken(),idDovecote ).subscribe((d:Dovecote)=>{
      this.dovecoteByIdObs.next(d)
    })

  }
  refresh() {
    if(this.user.getIdDovecote()!=null){
    this.http.getDovecote(this.user.getToken(), this.user.getIdDovecote()).subscribe((dovecote: Dovecote) => {
      this.dovecoteObs.next(dovecote);

    });
    }
  }

}




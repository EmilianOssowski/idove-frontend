import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";
import {SelectItem} from "primeng/api";
import {UserService} from "./user.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Pigeon} from "../models/pigeon.model";
import {Country} from "../models/country.model";
import {C} from "@angular/core/src/render3";

@Injectable()
export class CountryService {
  countriesObs = new BehaviorSubject<Array<Country>>([]);
  constructor(private http : HttpService, private user : UserService) {
    this.getCountries();
  }
  getCountries(){
    this.http.getCountries(this.user.getToken()).subscribe((res: Array<Country>)=>{
      this.countriesObs.next(res);

    })
  }
  getCountriesList(){
    return this.countriesObs.asObservable()
  }
}

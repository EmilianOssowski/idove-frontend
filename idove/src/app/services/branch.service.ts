import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Pigeon} from "../models/pigeon.model";
import {Branch} from "../models/branch.model";
import {HttpService} from "./http.service";
import {UserService} from "./user.service";

@Injectable()
export class BranchService {
  branchesListObs = new BehaviorSubject<Array<Branch>>([]);
  constructor(private http: HttpService, private user: UserService) {
    this.http.getBranches(this.user.getToken()).subscribe((res : Array<Branch>) =>{
      this.branchesListObs.next(res)
    })
  }


  getbranchesListObs(){
    return this.branchesListObs.asObservable()
  }
}

import { Injectable } from '@angular/core';
import {Branch} from "../models/branch.model";
import {UserService} from "./user.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {HttpService} from "./http.service";
import {Section} from "../models/section.model";

@Injectable()
export class SectionService {

  sectionsListObs = new BehaviorSubject<Array<Section>>([]);
  constructor(private http: HttpService, private user: UserService) {
  }
  refresh(idBranch){
    this.http.getSections(this.user.getToken(), idBranch).subscribe((res : Array<Section>) =>{
      this.sectionsListObs.next(res)
    })
  }

  getSectionListObs(){
    return this.sectionsListObs.asObservable()
  }


  addSection(idBranch, name){
    this.http.addSection(this.user.getToken(), idBranch, name);

  }

}

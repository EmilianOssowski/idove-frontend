import { Injectable } from '@angular/core';
import {Message, MessageService} from 'primeng/api';


@Injectable()
export class PopupsService {
  public msgs: Message[] = [];
  constructor(private messageService: MessageService) {

  }
  public addSingle() {
    this.messageService.add({severity: 'success', summary: 'Service Message', detail: 'Via MessageService'});
  }
  public addSuccess(title, detail) {
    this.messageService.add({severity: 'success', summary: title, detail: detail});
  }
  public addError(title, detail, life?) {
    this.messageService.add({severity: 'error', summary: title, detail: detail, life: life});
  }

  public addSuccessByEndpoint(title, endpoint) {
    switch (endpoint) {
      case ("dovecote"):{
        this.messageService.add({severity: 'success', summary: title, detail: "Dodano gołębnik"});
      }
    }

  }
}

import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Pigeon} from "../models/pigeon.model";
import {Observable} from "rxjs/Observable";
import {User} from "../models/user.model";
import {Dovecote} from "../models/dovecote.model";
import {loginJson} from "./oauth.service";
import {PopupsService} from "./popups.service";
import {SelectItem} from "primeng/api";
import {Section} from "../models/section.model";
import {error} from "util";
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {E} from "@angular/core/src/render3";
import {CFancier} from "../models/c-fancier.model";


@Injectable()
export class HttpService {

  url = "http://localhost:8080/";

  constructor(private http: HttpClient, private popup: PopupsService, private router : Router) {
  }

  getUserByToken(token): Observable<User> {

    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<User>('http://localhost:8080/user', {
      headers: headers
    });
  }
  login(username: string, password: string): Observable<Response> {
    let json: loginJson = new loginJson(username, password);
    let headers = new HttpHeaders({'Content-type': 'application/json'});
    return this.http.post('http://localhost:8080/login', JSON.stringify(json), {
      headers: headers
    }).map((res: Response) => {
      console.log(res);
      return res;
    })
  }

  refreshUserDetailsRequest : EventEmitter<boolean> = new EventEmitter<boolean>()

  changeDetails(token, body){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    this.http.put("http://localhost:8080/user",body,{headers: headers}).subscribe(res =>{
      this.popup.addSuccess("Sukces", "Dane zostały zmienione")
      this.refreshUserDetailsRequest.emit()
    },error1 => {
      this.popup.addError("Błąd", error1);
    })
  }
  changePassword(token, oldPassword, newPassword){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    this.http.put("http://localhost:8080/user/change-password",JSON.stringify({oldPassword: oldPassword,newPassword: newPassword}) ,{
      headers:headers
    }).subscribe(res =>{
      this.popup.addSuccess("Sukces", "Hasło zostało zmienione")
    }), err =>{
      this.popup.addError('Błąd', err)
    }

  }

  remindPassword(mail){
    this.http.get("http://localhost:8080/reset-password",{params : new HttpParams().set('userEmail',mail)}).subscribe(res=>{
      this.popup.addSuccess("Sukces", "Wysłano link na podany adres e-mail")
    },err => {
      this.popup.addError("Błąd", err.message)
    })
  }
  resetPassword(password, token){
    let headers: HttpHeaders = new HttpHeaders({'Content-type': 'application/json'});
    this.http.put("http://localhost:8080/change-password",JSON.stringify({'newPassword' : password}), {
      headers: headers,
      params: new HttpParams().set('token',token)}).subscribe(res =>{
      this.popup.addSuccess('Sukces', "Zmieniono hasło");
    },err=>{
      this.popup.addError('Błąd', err.message)
    })
  }

//dane hodowcy
  getFancierDetails(token){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    return this.http.get("http://localhost:8080/fancier", {
      headers: headers}).do(()=>{
        this.refreshDovecoteRequest.emit()
    })
  }

// Metody do gołębi
  getPigeons(token): Observable<Array<Pigeon>> {
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Array<Pigeon>>('http://localhost:8080/pigeon', {
      headers: headers
    });
  }

  refreshPigeonRequest : EventEmitter<boolean> = new EventEmitter<boolean>()

  addPigeon(token, pigeon: Pigeon) {
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token});
    this.http.post("http://localhost:8080/pigeon", pigeon, {
      headers: headers
    }).subscribe(pigeon => {
      this.popup.addSuccess("Sukces", "Dodano nowego gołębia");
      console.log(pigeon);
      this.refreshPigeonRequest.emit();
      console.log("Wysłano request")

    }), err => {
      this.popup.addError('Błąd', err.message)
    };
  }


  putPigeon(token, pigeon: Pigeon) {
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token});
    this.http.put("http://localhost:8080/pigeon", pigeon, {
      params: new HttpParams().set('pigeonId', pigeon.idPigeon.toString()),
      headers: headers
    }).subscribe(pigeon => {
      this.popup.addSuccess("Sukces", "Zmieniono dane gołębia");
      console.log(pigeon);
      this.refreshPigeonRequest.emit()
    }, err => {
      this.popup.addError('Błąd', err.message)
    });
  }

  deletePigeon(token, pigeon) {
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token});
    this.http.delete("http://localhost:8080/pigeon", {
      headers: headers,
      params: new HttpParams().set('pigeonId', pigeon.idPigeon.toString())
    }).subscribe(res => {
      this.popup.addSuccess('Sukces', 'Usunięto gołębia z bazy');
      this.refreshPigeonRequest.emit()
    }, err => {
      this.popup.addError('Błąd', err.message)
    })
  }




  getDovecote(token, idDovecote): Observable<Dovecote> {
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Dovecote>('http://localhost:8080/dovecote', {
      params: new HttpParams().set('idDovecote', idDovecote),
      headers: headers
    });
  }

  getCountries(token) {
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    return this.http.get("http://localhost:8080/country", {headers: headers

    })
  }

  //zwraca wszystkie oddziały
  getBranches(token){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    return this.http.get("http://localhost:8080/branch", {headers : headers})
  }
  //zwraca wszsytkie sekcje
  getSections(token, idBranch){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    return this.http.get<Array<Section>>("http://localhost:8080/section", {
      headers : headers,
    params: new HttpParams().set('idBranch',idBranch)})
  }

  getFanciersByEmail(token, email){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    return this.http.get("http://localhost:8080/user/section", { headers: headers,
      params: new HttpParams().set('email',email)})
  }


  //Metody dla rachmistrza
  getBranchList(token){ //oddziały którymi zarządza rachmistrz
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    return this.http.get("http://localhost:8080/branch/calculator", { headers: headers})
  }

  refreshSectionsRequest : EventEmitter<boolean> = new EventEmitter<boolean>()

  addSection(token, idBranch, name ){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    this.http.post("http://localhost:8080/section", JSON.stringify({idBranch:idBranch, name: name}),
      {headers: headers})
      .subscribe(res => {
        this.popup.addSuccess("Sukces", "Dodano nową sekcję");
        this.refreshSectionsRequest.emit();
    }), err => {
      this.popup.addError('Błąd', err.message)
    };
  }
  refreshFancierRequest : EventEmitter<boolean> = new EventEmitter<boolean>()
  addFancierToSection(token, userId, sectionId, fancierNumber){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    this.http.post("http://localhost:8080/fancier", JSON.stringify({userId:userId, sectionId:sectionId, fancierNumber:fancierNumber}), {
      headers: headers
    }).subscribe(()=>{
      this.popup.addSuccess("Sukces", "Dodano hodowcę do sekcji")
      this.refreshFancierRequest.emit()
    },err=>{
      this.popup.addError("Błąd", err.message)
    })
  }

  refreshDovecoteRequest : EventEmitter<boolean> = new EventEmitter<boolean>()
  //metoda uniwersalna
  postObject(token,endpoint, body){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    this.http.post("http://localhost:8080/"+endpoint,body,{headers: headers}).subscribe(()=>{
      this.popup.addSuccessByEndpoint("Sukces", endpoint)
      if(endpoint == 'dovecote'){
        this.refreshUserDetailsRequest.emit()
      }
    },err=>{
      this.popup.addError("Błąd", err.message)
    })
  }

  refreshDovecoteByIdRequest : EventEmitter<boolean> = new EventEmitter<boolean>()
  refreshPigeonsRequest : EventEmitter<boolean> = new EventEmitter<boolean>()
  //metoda uniwersalna
  putObject(token, endpoint, body ,parameter, parameterName , parameter2?){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    let params:HttpParams = new HttpParams().set(parameterName, parameter)
    this.http.put("http://localhost:8080/"+endpoint, body, {
      params: params,
      headers : headers
    }).subscribe(()=>{
      this.popup.addSuccess("Sukces", "Zakończono pomyślnie")
      if(endpoint == 'dovecote'){
        this.refreshUserDetailsRequest.emit()
      }
      if(endpoint == 'dovecote/confirm'){
        this.refreshDovecoteByIdRequest.emit()
      }
      if(endpoint == "pigeon/confirm"){
        this.refreshPigeonsRequest.emit(parameter2)
      }
    },error1 => {
      this.popup.addError("Błąd", error1.message);
    })
  }
  getObjectList(token, endpoint, parameter, parameterName){
    let headers: HttpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'});
    let params:HttpParams = new HttpParams().set(parameterName, parameter)
    return this.http.get<Array<any>>(this.url+endpoint,{headers: headers, params: params} )
  }


}

export interface Response {
  authToken: {
    token;
  },
  userDb: {
    idUser;
    username;
    password;
    email;
    confirmationToken;
    firstName;
    lastName;
    idSection;
    idDovecote;
    address;
    city;
    telephoneNumber;
    teamName;
    lastLogin;
    status;
    fancierNumber;
    registrationDate;
    fancier;
    calculator;
  }
}

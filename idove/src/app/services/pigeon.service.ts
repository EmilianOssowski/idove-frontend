import {Injectable, OnInit} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/Observable';
import {Pigeon} from '../models/pigeon.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {UserService} from './user.service';
import {Subscription} from "rxjs/Subscription";


@Injectable()
export class PigeonService {


  pigeonListObs = new BehaviorSubject<Array<Pigeon>>([]);
  constructor(private httpService: HttpService, private user: UserService) {
    // this.httpService.getPigeons(user.getToken(),user.getUserId() ).subscribe(list =>{
    //  this.pigeonListObs.next(list);
    //  console.log(list);
    // })

    this.httpService.refreshPigeonRequest.subscribe(
      ()=>{
        console.log("Odebrano request")
        this.refresh();
      }
    );

  }

  refreshRequest(){

  }


  refresh() {
    this.httpService.getPigeons(this.user.getToken()).subscribe(list => {
      this.pigeonListObs.next(list);
      console.log(list);
    });
  }

  getPigeonsListObs(): Observable<Array<Pigeon>> {       // GET
    return this.pigeonListObs.asObservable();
  }

  addPigeon(pigeon: Pigeon) {
    this.httpService.addPigeon(this.user.getToken(), pigeon);
  }
  editPigeon(pigeon : Pigeon){
    this.httpService.putPigeon(this.user.getToken(), pigeon);
  }


  deletePigeon(pigeon){
    this.httpService.deletePigeon(this.user.getToken(),pigeon)
  }

}

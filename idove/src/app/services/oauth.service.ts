import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import {PopupsService} from './popups.service';


export class loginJson {
  constructor(
    public username: string,
    public password: string) {
  }
}


@Injectable()
export class OauthService {

  constructor(
    private router: Router, private http: HttpClient, private cookie: CookieService, private popup : PopupsService) {
  }

  checkCredentials() {
    if (this.cookie.check('token')) {
      // this.router.navigate(['/login']);
      return true;
    } else { return false; }
  }

  logout() {
    this.cookie.deleteAll();

  }

}

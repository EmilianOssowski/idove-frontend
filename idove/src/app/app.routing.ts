import { Routes, RouterModule } from '@angular/router';
import {Pigeon} from './models/pigeon.model';
import {LoginComponent} from './components/login/login.component';



const appRoutes: Routes = [
  {path: 'login', component: LoginComponent },

  // otherwise redirect to profile
  { path: '**', redirectTo: '/login' }
];

export const routing = RouterModule.forRoot(appRoutes);

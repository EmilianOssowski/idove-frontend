export class User{
  public idUser : number;
  public firstName : string;
  public lastName : string;
  public password : string;
  public username : string ="";
  public address : string ;
  public city : string ;
  public email : string;
  public telephoneNumber : string ;
  public lastLogin = null;
  public confirmationToken : string ;

  public idSection : number;
  public idDovecote : number;
  public teamName : string ;
  public fancierNumber : number;
  public fancier : boolean = false;
  public calculator : boolean = false;

  constructor(idUser, firstName, lastName,  password,  username,  address,  city,  email,  telephoneNumber,  lastLogin,  confirmationToken  ){
    this.idUser = idUser;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.username = username;
    this.address = address;
    this.city = city;
    this.email = email;
    this.telephoneNumber = telephoneNumber;
    this.lastLogin = lastLogin;
    this.confirmationToken = confirmationToken;

  }
  // getidUser(){return this.idUser }
  // getusername(){return this.username }
  // getpassword(){return this.password }
  // getemail(){return this.email }
  // getconfirmationToken(){return this.confirmationToken }
  // getfirstName(){return this.firstName }
  // getlastName(){return this.lastName }
  // getidSection(){return this.idSection }
  // getidDovecote(){return this.idDovecote }
  // getaddress(){return this.address }
  // getcity(){return this.city }
  // gettelephoneNumber(){return this.telephoneNumber }
  // getteamName(){return this.teamName }
  // getlastLogin(){return this.lastLogin }
  // getstatus(){return this.status }
  // getfancierNumber(){return this.fancierNumber }
  // getfancier(){return this.fancier }
  // getcalculator(){return this.calculator }
}

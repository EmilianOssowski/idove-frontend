export class Branch{
  public idBranch :number = null;
  public branchNumber;
  public idRegion;
  public name : string = "";
  public idCalculator;

  constructor(idBranch, branchNumber, idRegion, name, idCalculator){
    this.idBranch = idBranch;
    this.branchNumber = branchNumber;
    this.idRegion = idRegion;
    this.name = name;
    this.idCalculator = idCalculator;
  }
 getIdBranch(){return this.idBranch;}
 getBranchNumber(){return this.branchNumber;}
 getIdRegion(){return this.idRegion;}
 getName(){return this.name;}
 getIdCalculator(){return this.idCalculator;}
}

export class Pigeon {
   public idPigeon: number;
   public idUser: number;
   public idCountry:number;
   public idColor:number;
   public branchNumber?: string;
   public yearbook?: string;
   public sex: boolean = false;
   public number?: string;
   public ringNumber: string;
   public isGMP?: boolean = false;
   public isMP?: boolean = false;
   public isIMP?: boolean = false;
   public confirm : boolean;


  constructor(idPigeon: number, idUser: number, idCountry: number, idColor: number, branchNumber: string,
              yearbook: string, sex: boolean, number: string, ringNumber: string, isGMP: boolean, isMP: boolean, isIMP: boolean){
  this.idPigeon = idPigeon;
  this.idUser = idUser;
  this.idCountry = idCountry;
  this.idColor = idColor;
  this.branchNumber = branchNumber;
  this.yearbook = yearbook;
  this.sex = sex;
  this.number = number;
  this.ringNumber = ringNumber;
  this.isGMP = isGMP;
  this.isMP = isMP;
  this.isIMP = isIMP;
  }



}

export class Country{
  idCountry : number;
  name : string;
  shortcut : string;
  constructor(idCountry, name, shortcut){
    this.idCountry= idCountry ;
    this.name= name ;
    this.shortcut= shortcut ;
  }

}

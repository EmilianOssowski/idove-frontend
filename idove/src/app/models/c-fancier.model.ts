import {Fancier} from "./fancier.model";

export class CFancier extends Fancier{
  idFancier;
  userId;
  sectionId;
  dovecoteId;
  fancierNumber;
  teamName;
  section: {
    idSection
    idBranch
    name
  };
    user: {
      idUser
      username
      password
      email
      confirmationToken
      firstName
      lastName
      address
      city
      telephoneNumber
      lastLogin
      registrationDate
      active
      calculator
      fancier
    }
  confirm


}

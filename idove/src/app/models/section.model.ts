export class Section {
  public idSection: number;
  public idBranch: number;
  public name : string;
  constructor(idSection, idBranch, name){
    this.idSection = idSection;
    this.idBranch = idBranch;
    this.name = name ;
  }
}

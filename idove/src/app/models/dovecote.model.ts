export class Dovecote {
  idDovecote: number;
  city: string;
  latitude: string;
  longtitude: string;
  adrdress: string;
  confirm : boolean;
  constructor(idDovecote: number, latitude, longtitude, address = '', city = '' ) {
    this.idDovecote = idDovecote ;
    this.city = city ;
    this.latitude = latitude;
    this.longtitude = longtitude;
    this.adrdress = address;
  }

  getLongtitudeNumber(){
    return Number(this.longtitude)
  }

  getLatitudeNumber(){
    return Number(this.latitude)
  }

}

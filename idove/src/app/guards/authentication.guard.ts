import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {UserService} from '../services/user.service';
import {OauthService} from '../services/oauth.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor( private user: UserService, private router: Router, private oauth: OauthService) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if(this.user.getIsUserLoggedIn()==false){
      this.router.navigate(['/login']);
      if(this.oauth.checkCredentials()==false){
      this.router.navigate(['/login']);
      return false;
      }
    } else {
      return true;
    }
  }
}

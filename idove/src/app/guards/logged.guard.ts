import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {OauthService} from '../services/oauth.service';
import {UserService} from '../services/user.service';

@Injectable()
export class LoggedGuard implements CanActivate {

  constructor( private user: UserService, private router: Router, private oauth: OauthService) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.oauth.checkCredentials() === true) {
        this.router.navigate(['/mainpage']);
        return false;
    } else { return true; }
  }
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {PigeonComponent} from './components/pigeon/pigeon.component';
import {HttpClientModule} from '@angular/common/http';
import {PigeonService} from './services/pigeon.service';
import {HttpService} from './services/http.service';
import {FormsModule} from '@angular/forms';
import {MainpageComponent} from './components/mainpage/mainpage.component';
import {RouterModule} from '@angular/router';
import {DovecoteComponent} from './components/dovecote/dovecote.component';
import {FilterPipe} from './pipes/filter.pipe';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {UserService} from './services/user.service';
import {AuthenticationGuard} from './guards/authentication.guard';
import {MenuComponent} from './components/menu/menu.component';
import {CookieService} from 'ngx-cookie-service';
import {OauthService} from './services/oauth.service';
import {RegisterService} from './services/register.service';
import {DovecoteService} from './services/dovecote.service';
import {ConfirmEmailComponent} from './components/confirm-email/confirm-email.component';
import {ConfigurationComponent} from './components/configuration/configuration.component';
import {ConfigurationService} from './services/configuration.service';
import {AddpigeonComponent} from './components/pigeon/addpigeon/addpigeon.component';
import {MessageModule} from "primeng/message";
import {
  AutoCompleteModule, CheckboxModule,
  ConfirmDialogModule,
  ContextMenuModule,
  DropdownModule, InputMaskModule, KeyFilterModule,
  MessagesModule,
  PanelMenuModule, PasswordModule, ProgressSpinnerModule,
  SelectButtonModule
} from "primeng/primeng";
import {ConfirmationService, MessageService} from "primeng/api";
import {MenuModule} from 'primeng/menu';
import {MenuItem} from 'primeng/api';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {DialogModule} from "primeng/dialog";
import {TableModule} from "primeng/table";
import {GMapModule} from "primeng/gmap";
import {PopupsService} from "./services/popups.service";
import {GrowlModule} from "primeng/growl";
import {CountryService} from "./services/country.service";
import {BranchService} from "./services/branch.service";
import {SectionService} from "./services/section.service";
import { CFancierComponent } from './components/c-fancier/c-fancier.component';
import {CFancierService} from "./services/cfancier.service";
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import {ExportService} from "./services/export.service";
import { CSectionComponent } from './components/c-section/c-section.component';
import { CVerificationComponent } from './components/c-verification/c-verification.component';
import {DataViewModule} from "primeng/dataview";
import {CalculatorService} from "./services/calculator.service";
import {PanelModule} from "primeng/panel";
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    AppComponent,
    PigeonComponent,
    MainpageComponent,
    DovecoteComponent,
    FilterPipe,
    LoginComponent,
    RegistrationComponent,
    MenuComponent,
    ConfirmEmailComponent,
    ConfigurationComponent,
    AddpigeonComponent,
    CFancierComponent,
    ResetPasswordComponent,
    CSectionComponent,
    CVerificationComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MessageModule,
    MessagesModule,
    RouterModule.forRoot([
      {path: '', redirectTo: '/login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'registration', component: RegistrationComponent},
      {path: 'confirm', component: ConfirmEmailComponent},
      {path: 'change-password', component: ResetPasswordComponent},
      {path: 'configuration', canActivate: [AuthenticationGuard], component: ConfigurationComponent},
      {path: 'mainpage', canActivate: [AuthenticationGuard], component: MainpageComponent},
      {path: 'pigeon', canActivate: [AuthenticationGuard], component: PigeonComponent},
      {path: 'dovecote', canActivate: [AuthenticationGuard], component: DovecoteComponent},
      {path: 'c-fancier', canActivate: [AuthenticationGuard], component: CFancierComponent},
      {path: 'c-section', canActivate: [AuthenticationGuard], component: CSectionComponent},
      {path: 'c-verification', canActivate: [AuthenticationGuard], component: CVerificationComponent},
    ]),
    //Prime NG
    MenuModule,ContextMenuModule, PanelMenuModule, BrowserAnimationsModule, DialogModule, TableModule, SelectButtonModule, DropdownModule,
    GMapModule, GrowlModule, ConfirmDialogModule, PasswordModule, ProgressSpinnerModule, AutoCompleteModule, DataViewModule, PanelModule,
    KeyFilterModule, InputMaskModule, CheckboxModule,
    //Export
    AgmCoreModule.forRoot({
      apiKey:"AIzaSyC2Nw_J4hIx8yGvQN5DDhRuoP_0oNt_FPc"
    })
  ],
  providers: [HttpService, PigeonService, UserService, AuthenticationGuard, CookieService, OauthService, RegisterService, DovecoteService,
    ConfigurationService, MessageService, PopupsService, ConfirmationService, CountryService, BranchService, SectionService, CFancierService,
    ExportService, CalculatorService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

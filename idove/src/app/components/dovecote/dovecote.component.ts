
import {Component, OnInit} from '@angular/core';

import {Dovecote} from "../../models/dovecote.model";
import {DovecoteService} from "../../services/dovecote.service";
import {UserService} from "../../services/user.service";
import {HttpService} from "../../services/http.service";
import {D} from "@angular/core/src/render3";
import {PopupsService} from "../../services/popups.service";

declare var google: any;

@Component({
  selector: 'app-dovecote',
  templateUrl: './dovecote.component.html',
  styleUrls: ['./dovecote.component.css']
})
export class DovecoteComponent implements OnInit {
  dovecote: Dovecote = new Dovecote(null, null, null, null, null);
  //mapa
  options: any;
  overlays: any[];
  lat: number = 52.4069200;
  lng: number = 19.9299300;
  markers : marker[] = [];
  zoom = 6;

  constructor(private dovecoteService: DovecoteService, private user: UserService, private http: HttpService, private popup : PopupsService) {
    if (this.user.getIdDovecote() != null) {
      this.dovecoteService.getDovecoteObs()
        .subscribe((dovecote: Dovecote) => {
          this.dovecote = dovecote;
          this.markers=[];
          this.markers.push({
            lat : Number(dovecote.latitude),
            lng : Number(dovecote.longtitude),
            label : dovecote.city + ", "+ dovecote.adrdress,
            draggable : false
          })
        });
      this.user.refreshMapRequest.subscribe(()=>{
        console.log("odebrano event refreshmap " +this.dovecote.idDovecote)
        this.dovecoteService.refresh();
        this.markers=[];
        this.markers.push({
          lat : Number(this.dovecote.latitude),
          lng : Number(this.dovecote.longtitude),
          label : this.dovecote.city + ", "+ this.dovecote.adrdress,
          draggable : false
        })
      })
    }

  }

  refreshMap(){
      this.markers=[];
      this.markers.push({
        lat : Number(this.dovecote.latitude),
        lng : Number(this.dovecote.longtitude),
        label : this.dovecote.city + ", "+ this.dovecote.adrdress,
        draggable : false
    })
  }


  ngOnInit() {
   //this.options = {
   //  center: {lat: 36.890257, lng: 30.707417},
   //  zoom: 12
   //};
  }

  isEditing: boolean = false;
  enableEditing(){
    this.isEditing = true;
  }
  editDovecote(){
    this.dovecoteService.editDovecote(this.dovecote);
    this.isEditing = false;
  }

  isAdding: boolean = false;
  enableAdding(){
    this.isAdding = true;
  }
  addDovecote(){
    if(this.dovecote.latitude != null && this.dovecote.longtitude != null && this.dovecote.adrdress != null && this.dovecote.city != null){
      let d : Dovecote = new Dovecote(null, this.dovecote.latitude, this.dovecote.longtitude, this.dovecote.adrdress, this.dovecote.city);
      this.dovecoteService.addDovecote(d);
      this.refreshMap();
      this.isAdding = false;

    }
    else {
      this.popup.addError("Błąd", "Podaj wszystkie dane");
    }
  }


}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DovecoteComponent } from './dovecote.component';

describe('DovecoteComponent', () => {
  let component: DovecoteComponent;
  let fixture: ComponentFixture<DovecoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DovecoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DovecoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

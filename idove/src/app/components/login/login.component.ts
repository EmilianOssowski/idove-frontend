import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {routing} from "../../app.routing";
import {Router, RouterModule} from "@angular/router";
import {UserService} from "../../services/user.service";
import {OauthService} from "../../services/oauth.service";
import {CookieService} from "ngx-cookie-service";
import {MessageService} from "primeng/api";
import {Message} from 'primeng//api';
import {PopupsService} from '../../services/popups.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username : string= 'r1';
  password : string= 'r1';
  msgs: Message[] = [];



  passwordForgot = false;
  email: string = "";
  constructor(private router : Router, private user : UserService) {
  }

  enablePasswordForgot(){
    this.passwordForgot = true;
  }
  login(){
    this.user.isLoginDisabled=true;
    this.user.login(this.username, this.password)
  }
  ngOnInit() {
  }
  closePasswordDialog(){
    this.passwordForgot = false;
  }
  sendPasswordLink(){
    this.user.remindPassword(this.email);
  }


}



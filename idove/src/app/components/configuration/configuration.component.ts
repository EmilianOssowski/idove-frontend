import { Component, OnInit } from '@angular/core';
import {ConfigurationService} from "../../services/configuration.service";
import {UserService} from "../../services/user.service";
import {PasswordModule} from 'primeng/password';
import {Branch} from "../../models/branch.model";


@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {
  //zmiana hasła
  isChangingPassword : boolean;
  oldPassword : string;
  newPassword : string;
  //zmiana danych
  isChangingDetails : boolean;
  username : string;
  password : string  = null;
  email : string;
  firstName : string;
  lastName : string;
  idSection : number;
  idDovecote : number;
  address : string;
  city : string;
  telephoneNumber : string;
//hodowca
  teamName : string;
  fancierNumber : number;
  isFancier : boolean;
  isCalculator : boolean;
//rachmistrz
  branchList : Array<Branch> = [];
  constructor(private user : UserService){
    this.username = this.user.getUsername();
    this.email = this.user.getEmail();
    this.firstName = this.user.getFirstName();
    this.lastName = this.user.getLastName();
    this.address = this.user.getAddress();
    this.city = this.user.getCity();
    this.telephoneNumber = this.user.getTelephoneNumber();
    this.teamName = this.user.getTeamName();
    this.fancierNumber = this.user.getFancierNumber();
    this.isFancier = this.user.isFancier();
    this.isCalculator = this.user.isCalculator();
    if(this.isFancier){
      this.teamName = this.user.getTeamName();
      this.fancierNumber = this.user.getFancierNumber();
      this.idSection = this.user.getIdSection();
      this.idDovecote = this.user.getIdDovecote();
    }
    if(this.isCalculator){
       this.user.getBranchList().subscribe( res =>{
         this.branchList=  res;
       });
    }
  }
  ngOnInit() {


  }


  changePasswordDialog(){
    this.isChangingPassword = !this.isChangingPassword;
  }
  public passwordRegex =/^[a-z0-9_-]{6,18}$/;
  isValid(){
    if(this.newPassword == "" || !this.passwordRegex.test(this.newPassword)){
    return false
    }else {
      return true;
    }
  }

  changePassword(){
    if(this.isValid()){
    console.log(this.oldPassword + "=>"+this.newPassword)
    this.user.changePassword(this.oldPassword, this.newPassword)
    this.changePasswordDialog();
    }
  }

  changeDetailsDialog(){
    this.isChangingDetails= ! this.isChangingDetails;
  }
  changeDetails(){
    this.user.changeDetails(this.username,
    this.email,
    this.firstName,
    this.lastName,
    this.idSection,
    this.idDovecote,
    this.address,
    this.city,
    this.telephoneNumber,
    this.teamName,
    this.fancierNumber
    )
    this.changeDetailsDialog()

  }
  clear(){
  this.username = null
  this.email = null
  this.firstName = null
  this.lastName = null
  this.idSection = null
  this.idDovecote = null
  this.address = null
  this.city = null
  this.telephoneNumber = null
  this.teamName = null
  this.fancierNumber = null
  this.isFancier = null
  this.isCalculator = null
  }



}

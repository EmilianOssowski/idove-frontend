import { Component, OnInit } from '@angular/core';
import {PasswordModule} from 'primeng/password';
import {UserService} from "../../services/user.service";
import {PopupsService} from "../../services/popups.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  isVisible: boolean = true;
  newPassword;
  copyNewPassword;

  resetPasswordToken;
  constructor(private user : UserService, private popup : PopupsService, private activatedRoute : ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.resetPasswordToken = params['token'];
      console.log(this.resetPasswordToken); // Print the parameter to the console.
    });
  }

  ngOnInit() {
  }
  public passwordRegex =/^[a-z0-9_-]{6,18}$/;
  isValid(){
    if(this.newPassword == "" ||!this.passwordRegex.test(this.newPassword))
      return false;
    else return true;
  }


  changePassword(){
    if(this.newPassword != this.copyNewPassword)
    {
      this.popup.addError("Błąd","Hasła się nie zgadzają")
    }
    else if(this.isValid()) {
      this.popup.addError("Błąd", "Hasło nie spełnia wymagań (6-18 znaków)")
    }

    else{
    this.user.resetPassword(this.newPassword ,this.resetPasswordToken)
    }

}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CVerificationComponent } from './c-verification.component';

describe('CVerificationComponent', () => {
  let component: CVerificationComponent;
  let fixture: ComponentFixture<CVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

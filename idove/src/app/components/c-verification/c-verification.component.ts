import { Component, OnInit } from '@angular/core';
import {CFancier} from "../../models/c-fancier.model";
import {CalculatorService} from "../../services/calculator.service";
import {Section} from "../../models/section.model";
import {SelectItem} from "primeng/api";
import {Dovecote} from "../../models/dovecote.model";
import {DovecoteService} from "../../services/dovecote.service";
import {Pigeon} from "../../models/pigeon.model";

@Component({
  selector: 'app-c-verification',
  templateUrl: './c-verification.component.html',
  styleUrls: ['./c-verification.component.css']
})
export class CVerificationComponent implements OnInit {
  sortedFancierList : Array<CFancier> ;
  fancierList : Array<CFancier>;

  selectedFancier : CFancier;
  selectedDovecote: Dovecote;
  selectedPigeon : Pigeon;
  selectedPigeons : Array<Pigeon>;

  sectionList : SelectItem[] = [];
  selectedSection : Section;

  isDovecoteVerify = false;
  isPigeonsVerify= false;
  constructor(private calculator : CalculatorService, private dovecote : DovecoteService) {  }

  ngOnInit() {
    this.sortedFancierList = this.fancierList;
    this.calculator.getFanciersListObs().subscribe(res => {
      this.fancierList = res;
      this.sortedFancierList = res;
    })
    this.dovecote.getDovecoteByIdObs().subscribe((d:Dovecote)=>{
      this.selectedDovecote = d;
      console.log(d)
    })
    this.calculator.getPigeonsListObs().subscribe((p: Array<Pigeon>)=>{
      this.selectedPigeons = p;
    })
  }

  getSectionList() {
    this.calculator.getSectionsListObs().subscribe((res: Array<Section>) => {
      this.sectionList = [];
      for (let sec of res) {
        this.sectionList.push({label: sec.name, value: sec})
      }
      //console.log(this.sectionList)
    })
  }

  onSortChange(event) {

    if(event == null){
      this.sortedFancierList = this.fancierList;
    }else{
      this.sortedFancierList = [];
      let value = event.idSection;
      this.fancierList.forEach((f : CFancier)=>{
        if (f.sectionId == value) {
          this.sortedFancierList.push(f);
        }
      })
    }
  }
  confirmDovecote(){
    this.calculator.confirmDovecote(this.selectedDovecote.idDovecote)
  }

  selectDovecote($event, fancier) {
    console.log(fancier);
    this.selectedFancier = fancier;
    this.isDovecoteVerify = true;
    this.dovecote.getDovecoteById(this.selectedFancier.dovecoteId)
  }


  selectPigeons($event, fancier) {
    this.selectedFancier = fancier;
    this.calculator.getPigeonsByIdFancier(this.selectedFancier.idFancier)
    this.isPigeonsVerify = true;
  }
  confirmPigeon(pigeon){
    this.calculator.confirmPigeon(pigeon.idPigeon, this.selectedFancier.idFancier)
  }
}

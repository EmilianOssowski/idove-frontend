import {Component, OnInit} from '@angular/core';
import {CFancier} from "../../models/c-fancier.model";
import {CalculatorService} from "../../services/calculator.service";
import {Section} from "../../models/section.model";
import {SelectItem} from "primeng/api";
import {User} from "../../models/user.model";

@Component({
  selector: 'app-c-fancier',
  templateUrl: './c-fancier.component.html',
  styleUrls:
['./c-fancier.component.css']
})

export class CFancierComponent implements OnInit {
  fancierList: Array<CFancier>;
  sortedFancierList : Array<CFancier> ;

  selectedSection: Section;
  sectionList: SelectItem[] = [];
  selectedSectionInAdding;

  isAdding: boolean = false;
  email: string;

  isLoading = true;

  selectedUser: User = new User(1, "", "", null, '', " ", " ", "", "", null, null);
  fancierNumber: number;

  selectedFancier: CFancier;
  isInfo: boolean = false;

  constructor(private calculator: CalculatorService) {
  }

  ngOnInit() {
    this.sortedFancierList = this.fancierList;
    this.calculator.getFanciersListObs().subscribe(res => {
      this.fancierList = res;
      this.sortedFancierList = res;
    })
  }

  getSectionList() {
    this.calculator.getSectionsListObs().subscribe((res: Array<Section>) => {
      this.sectionList = [];
      for (let sec of res) {
        this.sectionList.push({label: sec.name, value: sec})
      }
      //console.log(this.sectionList)
    })
  }

  enableAddingFancier() {
    this.isAdding = true;

  }

  onSortChange(event) {

    if(event == null){
      this.sortedFancierList = this.fancierList;
    }else{
      this.sortedFancierList = [];
      let value = event.idSection;
      this.fancierList.forEach((f : CFancier)=>{
        if (f.sectionId == value) {
          this.sortedFancierList.push(f);
        }
      })
    }


  }
  selectFancier($event, fancier) {
    this.selectedFancier = fancier;
    this.isInfo = true;
  }

  addFancier() {
    this.calculator.addFancier(this.selectedUser.idUser, this.selectedSectionInAdding.idSection, this.fancierNumber)
    this.isAdding=false;
    this.selectedSection = null
  }

  searchUser() {

  }


  results: Array<User>;

  search(event) {
    let query = event.query;
    this.calculator.getFanciersByEmail(query).subscribe((res: Array<User>) => {
      console.log(res)
      this.results = res;
      //this.results = this.filterUsers(query, res)
    });
    //this.results = this.filterUsers(query, this.userList);
  }

  filterUsers(query, users: Array<User>): any[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    for (let i = 0; i < users.length; i++) {
      let user = users[i];
      if (user.email.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(user);
      }
    }
    return filtered;
  }
}

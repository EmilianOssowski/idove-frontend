import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CFancierComponent } from './c-fancier.component';

describe('CFancierComponent', () => {
  let component: CFancierComponent;
  let fixture: ComponentFixture<CFancierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CFancierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CFancierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

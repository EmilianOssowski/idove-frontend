import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../../services/register.service';
import {User} from '../../models/user.model';
import {UserService} from '../../services/user.service';
import {PopupsService} from "../../services/popups.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  //1 etap
  private username : string   =''        //= "kk";
  private password : string =''          //= "bartek";
  private email : string =''             //= "lvzik12@gmail.com";
  private firstName : string          //= "Krzysztof";
  private lastName : string           //= "Krawczyk";
  private address : string            //= "Rokietnica 53";
  private city : string               //= "Rokietnica";
  private telephoneNumber : string    //= "555555123";

  //validator
  public emailRegex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public passwordRegex =/^[a-z0-9_-]{6,18}$/;
  public usernameRegex = /^[a-z0-9_-]{3,16}$/;
  public number : number;
  isEmailValid : boolean = true;


  isValid(){
    if(!this.emailRegex.test(this.email) ||
      this.email==""||
      !this.usernameRegex.test(this.username)||
      this.username == ""||
      this.password == "" ||
      !this.passwordRegex.test(this.password))
      return false;
    else return true;
  }

  constructor(private registerService : RegisterService, private popup : PopupsService) { }

  ngOnInit() {
  }
  register() {
      if (this.isValid()) {
        this.registerService.register(this.username, this.password, this.email, this.firstName, this.lastName,
          this.address, this.city, this.telephoneNumber);
      }
      else {
        let details :string  = "Błędne pola:  "+"<br />";
        if(!this.usernameRegex.test(this.username )|| this.username == "") details = details + "- Login (3-16 znaków) "+"<br />";
        if(!this.emailRegex.test(this.email )|| this.email == "") details = details + "- E-mail "+"<br />";
        if(!this.passwordRegex.test(this.password )|| this.password == "") details = details + "- Hasło (6-18 znaków) "+"<br />";
        this.popup.addError("Błąd", "Nieprawidłowo wypełnione dane"+"<br />" + details, 10000)
      }

  }
  validate(){

  }


}



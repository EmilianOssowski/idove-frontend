import {AfterViewInit, Component, OnChanges, OnInit} from '@angular/core';
import {SectionService} from "../../services/section.service";
import {Section} from "../../models/section.model";
import {CalculatorService} from "../../services/calculator.service";

@Component({
  selector: 'app-c-section',
  templateUrl: './c-section.component.html',
  styleUrls: ['./c-section.component.css']
})
export class CSectionComponent implements OnInit{
  constructor(private calculator : CalculatorService) { }
  sectionsList  = [];

  isAdding : boolean= false ;
  sectionName : string = "";

  isInfo : boolean = false;
  selectedSection : Section = new Section(null,null,"");
  ngOnInit() {
    this.calculator.getSectionsListObs()
      .subscribe((sections : Array<Section>) =>{
        this.sectionsList=sections
      })
  }
  enableAddingSection(){
    this.isAdding=true;
    this.sectionName= ""
  }
  addSection(){
    this.calculator.addSection(this.sectionName);
    this.isAdding=false;


  }

  selectSection($event, section){
    this.selectedSection = section;
    this.isInfo = true;
  }


}



import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {Url} from "url";
import {ActivatedRoute, Router} from "@angular/router";
import {PopupsService} from "../../services/popups.service";

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {
  confirmationToken : string;
  constructor( private http : HttpClient, private activatedRoute: ActivatedRoute, private router : Router, private popup : PopupsService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.confirmationToken = params['token'];
      console.log(this.confirmationToken);
      this.confirmEmail()// Print the parameter to the console.
    });

  }

  ngOnInit() {
  }

  confirmEmail() {

    let headers = new HttpHeaders({ 'Content-type': 'application/json'});
    var params = new HttpParams();
    params.append('confirmationToken', this.confirmationToken);
    this.http.get('http://localhost:8080/confirm', {params : new HttpParams().append('confirmationToken', this.confirmationToken),headers: headers })
      .subscribe(
        data => {
          this.popup.addSuccess("Sukces", "Potwierdzono adres e-mail");
          this.router.navigate(['/mainpage']);
          },
       (err :HttpErrorResponse) => {
          this.popup.addError("Błąd", err.error.message);
        });
  }
}

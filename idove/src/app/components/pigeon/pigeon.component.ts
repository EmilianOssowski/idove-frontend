import {Component, OnInit, ViewChild} from '@angular/core';
import {PigeonService} from "../../services/pigeon.service";
import {Pigeon} from "../../models/pigeon.model";
import {AddpigeonComponent} from "./addpigeon/addpigeon.component";
import {ConfirmationService} from 'primeng/api';
import {Country} from "../../models/country.model";
import {CountryService} from "../../services/country.service";
import {ExportService} from "../../services/export.service";


@Component({
  selector: 'app-pigeon',
  templateUrl: './pigeon.component.html',
  styleUrls: ['./pigeon.component.css']
})
export class PigeonComponent implements OnInit{
  //POLA INPUT
  isAdding : boolean = false;
  showDeleteConfirmation : boolean = false;
  pigeonList = [];
  searchString: string = "";

  selectedPigeon : Pigeon ;

  countriesList : Array<Country> = [];

  @ViewChild(AddpigeonComponent)
  private dialog: AddpigeonComponent;




  constructor( private pigeonService: PigeonService, private  confirmationService : ConfirmationService, private country : CountryService, private exportService : ExportService) {

  }
  ngOnInit() {
    this.pigeonService.getPigeonsListObs()
      .subscribe((pigeons: Array<Pigeon>) => {
        this.pigeonList = pigeons;
      });
    this.getCountries();


  }

  getCountries(){
    this.country.getCountriesList().subscribe((res : Array<Country>)=>{
      this.countriesList=res;
      console.log(res);
    } )

  }



  confirmDelete(pigeon){
    this.confirmationService.confirm({
      message: 'Czy napewno chcesz usunąć tego gołębia',
      accept: () => {
        this.pigeonService.deletePigeon(pigeon);
      },
      reject : ()=>{

      },
      acceptLabel : 'Tak',
      rejectLabel: 'Nie',


    });
  }

  //Buttons controllers
  enableAdding(){
    this.dialog.clearInputs();
    this.dialog.isAdding= true;
    this.isAdding = !this.isAdding;
  }
  enableEdit(pigeon){
    console.log(pigeon);
    this.dialog.isAdding= false;
    this.dialog.selectedPigeon = pigeon;
    this.dialog.fillInputs();
    this.isAdding = true ;
  }




  refresh() {
    this.pigeonService.refresh();
  }


  closeDialogReceive($event){
    this.isAdding = $event
  }

  exportList(){
    var indexLp = 0;
    const exportPigeonList = [];
    this.pigeonList.forEach( (val : Pigeon) =>{
      let p : PigeonView = new PigeonView();
      indexLp = indexLp +1
      p.Lp = indexLp;
      p.Nr_Obraczki= val.ringNumber;
      this.dialog.colors.forEach( value => {
        if(value.value == val.idColor) p.kolor=value.label
      });
      p.plec = val.sex;
      exportPigeonList.push(p);
    });
    this.exportService.exportAsExcelFile(exportPigeonList, 'Lista gołębi');

  }
}
export class PigeonView {
  Lp;
  Nr_Obraczki;
  plec;
  kolor;
  idPigeon
}

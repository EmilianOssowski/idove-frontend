import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pigeon} from "../../../models/pigeon.model";
import {PigeonService} from "../../../services/pigeon.service";
import {SelectItem} from "primeng/api";
import {CountryService} from "../../../services/country.service";
import {Country} from "../../../models/country.model";
import {AutoCompleteModule} from 'primeng/autocomplete';



@Component({
  selector: 'app-addpigeon',
  templateUrl: './addpigeon.component.html',
  styleUrls: ['./addpigeon.component.css']
})
export class AddpigeonComponent implements OnInit {
  private idPigeon: number;
  private idUser: number;
  private idCountry: number;
  private idColor: number;
  private branchNumber: string;
  private yearbook: string;
  private sex: boolean;
  private number: string;
  private ringNumber: string;
  private isGMP: boolean;
  private isMP: boolean;
  private isIMP: boolean;

  //PrimeNG
  sexChoose;
  colors: SelectItem[];
  @Output() closeDialog = new EventEmitter<boolean>();

  selectedCountry : Country;
  @Input() countries : Array<Country>;
  filteredCountries : Array<Country>;

  isAdding : boolean;

  selectedPigeon: Pigeon;

  constructor( private pigeonService : PigeonService ,private country: CountryService) {
    this.sexChoose = [
      {label:'Samiec', value:true},
      {label:'Samica', value:false},
    ];
    this.colors = [
      {label:'NIEB niebieska',value:1	},
      {label:'NNAK niebieska nakrapiana',value:2	},
      {label:'CIEM ciemna ciemna',value:3	},
      {label:'CZER czerwona',value:4	},
      {label:'PLOW płowa',value:5	},
      {label:'CZAR czarna czarna',value:6	},
      {label:'SZPA szpakowata',value:7	},
      {label:'CZEN czerwona nakrapiana',value:8	},
      {label:'CNAK ciemna nakrapiana',value:9	},
      {label:'BIAL biała',value:10},
      {label:'NIEP niebieska pstra',value:11},
      {label:'NNAP niebieska nakrapiana',value:12},
      {label:'CIEP ciemna pstra',value:13},
      {label:'CZEP czerwona pstra',value:14},
      {label:'PLOP płowa pstra',value:15},
      {label:'CZAP czarna pstra',value:16},
      {label:'SZPP szpakowata pstra',value:17},
      {label:'CZNP czerwona nakrapiana',value:18},
      {label:'CNAP ciemna nakrapiana',value:19},
      {label:'CZES czerwona szpakowata',value:20}
    ];

  }

  ngOnInit() {
  }





  searchCountries(event) {
    let query : string = event.query;
    this.filterCountries(query)
  }
  filterCountries(query : string) {
    console.log(query)
    this.filteredCountries  = [];
    for(let i = 0; i < this.countries.length; i++) {
      let branch = this.countries[i];
      if(branch.shortcut.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        this.filteredCountries.push(branch);
      }
    }

  }
  clearCountries(){
    delete this.countries;
    console.log(this.countries)
  }


  add() {
    const p: Pigeon = new Pigeon(
      null,
      null,
      this.selectedCountry.idCountry,
      this.idColor,
      this.branchNumber,
      this.yearbook,
      this.sex = false,
      this.number,
      null,
      this.isGMP = false,
      this.isMP = false,
      this.isIMP = false,
    );
    this.pigeonService.addPigeon(p);
    this.clearInputs();
    this.closeDialog.emit(false);

  }
  edit(){
    console.log(this.selectedPigeon);
    const p: Pigeon = new Pigeon(
      this.selectedPigeon.idPigeon,
      this.selectedPigeon.idUser,
      this.idCountry,
      this.idColor,
      this.branchNumber,
      this.yearbook,
      this.sex = false,
      this.number,
      null,
      this.isGMP = false,
      this.isMP = false,
      this.isIMP = false,
    );
    this.pigeonService.editPigeon(p);
    this.clearInputs();
    console.log("wysłano event closeDialog");
    this.closeDialog.emit(false);
  }


  fillInputs(){
    this.idPigeon = this.selectedPigeon.idPigeon;
    this.idUser = this.selectedPigeon.idUser;
    this.idCountry = this.selectedPigeon.idCountry;
    this.idColor = this.selectedPigeon.idColor;
    this.branchNumber = this.selectedPigeon.branchNumber;
    this.yearbook = this.selectedPigeon.yearbook;
    this.sex = this.selectedPigeon.sex;
    this.number = this.selectedPigeon.number;
    this.ringNumber = this.selectedPigeon.ringNumber;
    this.isGMP = this.selectedPigeon.isGMP;
    this.isMP = this.selectedPigeon.isMP;
    this.isIMP = this.selectedPigeon.isIMP;
  }


  clearInputs(): void {
    this.idPigeon = null;
    this.idUser = null;
    this.idCountry = null;
    this.idColor = null;
    this.branchNumber = "";
    this.yearbook = "";
    this.sex = false;
    this.number = "";
    this.ringNumber = "";
    this.isGMP = false;
    this.isMP = false;
    this.isIMP = false;

  }
}

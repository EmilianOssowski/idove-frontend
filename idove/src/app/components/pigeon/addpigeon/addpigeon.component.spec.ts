import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpigeonComponent } from './addpigeon.component';

describe('AddpigeonComponent', () => {
  let component: AddpigeonComponent;
  let fixture: ComponentFixture<AddpigeonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddpigeonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddpigeonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

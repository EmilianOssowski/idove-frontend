import {AfterViewChecked, Component, DoCheck, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {UserService} from "../../services/user.service";
import {PigeonService} from "../../services/pigeon.service";
import {DovecoteService} from "../../services/dovecote.service";
import {MenuItem} from "primeng/api";
import {MenuModule} from 'primeng/menu';
import {PanelMenuModule} from 'primeng/panelmenu';
import {Router, RouterModule} from "@angular/router";
import {Branch} from "../../models/branch.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {CalculatorService} from "../../services/calculator.service";
import {CSectionComponent} from "../c-section/c-section.component";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {

  items: MenuItem[];
  itemsCalculator : Array<MenuItem[]> = [[]];
  itemsFancier : MenuItem[] = [];
  branchList : BehaviorSubject<Array<Branch>> = new BehaviorSubject<Array<Branch>>([]);
  constructor(private router : Router, private user : UserService, private pigeon : PigeonService, private dovecote : DovecoteService, private calculator: CalculatorService) {
  }

  ngOnInit() {
    this.refresh2()


  }
  refresh2() {
    this.itemsCalculator = [];
    this.items = [
      {
        label: 'IDove',
        icon: 'pi pi-pw pi-cloud',
        items: [
          {
            label: 'Strona główna', icon: 'pi pi-fw pi-home', command: (click) => {
              this.router.navigate(["/mainpage"]);
            }
          },
          {separator: true},
          {
            label: 'Ustawienia', icon: 'pi pi-fw pi-cog', command: (click) => {
              this.router.navigate(["/configuration"]);
            }
          },
        ]
      }]
    this.itemsFancier = [
      {
        label: 'Hodowca',
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'Gołębie', icon: 'pi pi-fw pi-angle-right', command: (click) => {
              this.pigeon.refresh();
              this.router.navigate(["/pigeon"]);
            }
          },
          {separator: true},
          {
            label: 'Gołębnik', icon: 'pi pi-fw pi-inbox', command: (click) => {
              this.dovecote.refresh();
              this.router.navigate(["/dovecote"]);

            }
          },
        ]
      }]
    this.user.getBranchList().subscribe((res: Array<Branch>) => {
      for (let val of res) {

        var item: MenuItem[];
        item = [
          {
            label: 'Rachmistrz - ' + val.name,
            icon: 'pi pi-fw pi-user',
            items: [
              {
                label: 'Oddział',  //lista hodowców
                icon: 'pi pi-fw pi-th-large',
                command: (click) => {
                  console.log(val)
                },
                items: [
                  {
                    label: 'Sekcje', icon: 'pi pi-fw pi-circle-off', command: (click) => {
                      this.calculator.clearBranch()
                      this.calculator.setBranch(val)
                      this.router.navigate(["/c-section"]);
                    }
                  },
                  {
                    label: 'Hodowcy', icon: 'pi pi-fw pi-users', command: (click) => {
                      this.calculator.clearBranch()
                      this.calculator.setBranch(val)
                      this.router.navigate(["/c-fancier"])
                    }
                  },
                ]
              },
              {
                label: 'Weryfikacja', icon: 'pi pi-fw pi-check', command: (click) => {
                  this.calculator.clearBranch()
                  this.calculator.setBranch(val)
                  this.router.navigate(["/c-verification"]);
                }
              }
            ]
          }
        ];

        this.itemsCalculator.push(item);
      }
    });


  }

}


import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {OauthService} from "../../services/oauth.service";
import {PopupsService} from "../../services/popups.service";
import {Message} from "primeng/api";
import {Branch} from "../../models/branch.model";
import {BranchService} from "../../services/branch.service";
import {AutoCompleteModule} from 'primeng/autocomplete';
import {Section} from "../../models/section.model";
import {SectionService} from "../../services/section.service";



@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {

  branchesList : Array<Branch> = [new Branch(1,'08',1, 'gorzów',null)];
  selectedBranch : Branch = new Branch(null,null,null,null,null);
  filteredBranchList: Array<Branch>;

  sectionList : Array<Section> = [];
  selectedSection : Section = new Section( null , null ,null)
  filteredSectionList: Array<Section>= [];
  constructor(private popup : PopupsService, private branch : BranchService, private section : SectionService) {

  }
  ngOnInit() {
    //this.getBranches();
  }
  getSections(){
    this.section.refresh(this.selectedBranch.idBranch)
    this.section.getSectionListObs().subscribe((res)=>{
      this.sectionList = res;
      console.log(this.sectionList);
    })
  }

  searchSections(event) {

    let query : string = event.query;
    this.filterSections(query)
  }
  filterSections(query : string) {
    console.log(query);
    this.filteredSectionList  = [];
    for(let i = 0; i < this.sectionList.length; i++) {
      let section = this.sectionList[i];
      if(section.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        this.filteredSectionList.push(section);
      }
    }

  }
  checkIfExist(){

    console.log("tu dajemy dodawanie sekcji")

  }

  getBranches(){
    this.branch.getbranchesListObs().subscribe((res)=>{
      this.branchesList = res;
      console.log(this.branchesList);
    })
  }
  searchBranches(event) {
    let query : string = event.query;
    this.filterBranches(query)
  }
  filterBranches(query : string) {
    console.log(query)
    this.filteredBranchList  = [];
    for(let i = 0; i < this.branchesList.length; i++) {
      let branch = this.branchesList[i];
      if(branch.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        this.filteredBranchList.push(branch);
      }
    }

  }
  clearBranch(){
    delete this.branchesList;
    console.log(this.branchesList)
  }


}

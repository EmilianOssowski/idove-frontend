import {Component, OnDestroy} from '@angular/core';
import {PigeonService} from "./services/pigeon.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Pigeon} from "./models/pigeon.model";
import {Observable} from "rxjs/Observable";
import {UserService} from "./services/user.service";
import {Router, RouterModule} from "@angular/router";
import {OauthService} from "./services/oauth.service";
import {MessageService} from "primeng/api";
import {PopupsService} from "./services/popups.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  private isUserLogged: boolean;
  constructor(private userService: UserService, private router: Router, private popup: PopupsService) {
    this.isUserLogged = this.userService.getIsUserLoggedIn();
    console.log(this.isUserLogged);
  }

  logout(){
    this.userService.logout();
    this.router.navigate(['/login']);

  }
  ngOnDestroy() {
    this.userService.logout();
  }
}
